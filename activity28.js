// 3. Single Room (insertOne Method)
db.users.insert({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic neccessities",
	roomsAvailable: 10,
	isAvailable: false
});

// 4. Multiple Room (insertMany Method)
db.users.insert([
{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	roomsAvailable: 5,
	isAvailable: false
},
{
	name: "double",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	roomsAvailable: 15,
	isAvailable: false
}
]);

// 5. Find Method
db.users.find({
	name: "double"
});

// 6. updateOne Method
db.users.updateOne(
	{ description: "A room with a queen sized bed perfect for a simple getaway" },
	{
		$set: { roomsAvailable: 0 },
	}
);

// 7. deleteMany Method
db.users.deleteMany({
	roomsAvailable: 0
});